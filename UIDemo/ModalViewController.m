//
//  ModalViewController.m
//  UIDemo
//
//  Created by YOSHIDA Kaname on 2015/01/19.
//  Copyright (c) 2015年 YOSHIDA Kaname. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *statusKeyList;
@property (strong, nonatomic) NSDictionary *statusDict;

@property (strong, nonatomic) NSString *status;

@end

@implementation ModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.status = @"---";
    
    self.statusList = @[@"未定義", @"読みたい", @"未読", @"読書中", @"読了", @"廃棄"];
    self.statusKeyList = @[@"---", @"WISH", @"BOUGHT", @"READING", @"READ", @"DEPOSE"];
    self.statusDict = [[NSDictionary alloc] initWithObjects:self.statusList forKeys:self.statusKeyList];
    
    // UIPickerView
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- UIPickerViewDelegate UIPickerViewDataSource --
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.statusList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.statusList[row];
}

#pragma mark -- UIViewController処理 --
- (IBAction)pushCancelButton:(UIButton *)sender
{
    //
    NSLog(@"Cancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pushSettingButton:(UIButton *)sender
{
    //
    NSLog(@"Setting");
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)pushBackgroundButton:(UIButton *)sender
{
    //
    NSLog(@"Background");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.status = self.statusKeyList[row];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
