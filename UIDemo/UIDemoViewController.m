//
//  ViewController.m
//  UIDemo
//
//  Created by YOSHIDA Kaname on 2015/01/18.
//  Copyright (c) 2015年 YOSHIDA Kaname. All rights reserved.
//

#import "UIDemoViewController.h"
#import "ModalViewController.h"

@interface UIDemoViewController () <UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) NSArray *statusList;
@property (strong, nonatomic) NSArray *statusKeyList;
@property (strong, nonatomic) NSDictionary *statusDict;

@property (strong, nonatomic) NSString *status;

@end

@implementation UIDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.status = @"---";
    self.statusList = @[@"未定義", @"読みたい", @"未読", @"読書中", @"読了", @"廃棄"];
    self.statusKeyList = @[@"---", @"WISH", @"BOUGHT", @"READING", @"READ", @"DEPOSE"];
    self.statusDict = [[NSDictionary alloc] initWithObjects:self.statusList forKeys:self.statusKeyList];
    
    // Label設定
    self.statusLabel.text = [self.statusDict objectForKey:self.status];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- UIViewControllerでの表示処理 --
/**
 ModalViewControllerを表示
 */
- (IBAction)pushDisplayModal:(UIButton *)sender
{
    //ModalViewController *modalController = [[self storyboard] instantiateViewControllerWithIdentifier:@"ModalViewController"];
    ModalViewController *modalController = [[ModalViewController alloc] initWithNibName:@"ModalViewController" bundle:nil];
    modalController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:modalController animated:YES completion:nil];
    
}

#pragma mark -- UIAlertControllerでの表示処理 --
/**
 アクションシート (UIAlartController)
 */
- (IBAction)pushDisplayAlartSheet:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"状態選択" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSString *status in self.statusKeyList) {
        [alertController addAction:[UIAlertAction actionWithTitle:self.statusDict[status] style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            self.status = status;
            self.statusLabel.text = [self.statusDict objectForKey:self.status];
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:@"キャンセル" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -- UIActionSheetでの表示処理 --
/**
 アクションシート (UIActionSheet)
 */
- (IBAction)pushDisplayActionSheet:(UIButton *)sender
{
    //UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"状態選択" delegate:nil cancelButtonTitle:@"キャンセル" destructiveButtonTitle:nil otherButtonTitles:@"未定義", @"読みたい", @"未読", @"読書中", @"読了", @"廃棄", nil];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"状態選択" delegate:self cancelButtonTitle:@"キャンセル" destructiveButtonTitle:nil otherButtonTitles:nil];
    for (NSString *status in self.statusList) {
        [actionSheet addButtonWithTitle:status];
    }
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"buttonIndex:%ld", (long)buttonIndex);
    
    if (buttonIndex == 0) {
        // cancel処理
    }
    else {
        self.status = [self.statusKeyList objectAtIndex:(buttonIndex-1)];
        self.statusLabel.text = [self.statusDict objectForKey:self.status];
    }
}

@end
